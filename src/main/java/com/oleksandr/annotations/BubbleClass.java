package com.oleksandr.annotations;

@Bubble(name = "class")
public class BubbleClass {
    public static String emptyField;

    @Bubble(name = "bubble :D")
    public String bubble;

    BubbleClass() {
       bubble = "bubble :D";
    }
}
