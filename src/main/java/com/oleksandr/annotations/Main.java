package com.oleksandr.annotations;


public class Main {
    public static void main(String[] args) {
        ReflectionWorker ref = new ReflectionWorker();
        Worker worker = new Worker();
        ref.showInformationAboutClass(worker);
    }
}
