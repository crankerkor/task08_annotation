package com.oleksandr.annotations;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ReflectionWorker {

    public void printOnlyBubbles() {
        Class bubble = BubbleClass.class;
        Field[] fields = bubble.getDeclaredFields();
        BubbleClass bubbleInstance = new BubbleClass();

        for (Field field : fields) {
            if (field.isAnnotationPresent(Bubble.class)) {
                field.setAccessible(true);

                try {
                System.out.println(field.get(bubbleInstance));
                } catch(IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void printAnnotationValue() {
        Class bubble = BubbleClass.class;
        Class bubbleAnnotation = Bubble.class;
        try {
            Field field = bubble.getField("bubble");
            Bubble annotation = (Bubble) field.getAnnotation(bubbleAnnotation);
            Method name = bubbleAnnotation.getDeclaredMethod("name");

            System.out.println("@" + bubbleAnnotation.getSimpleName() +
                    "(" + name.getName() + "=" + annotation.name() + ")");

        } catch (Exception nsf) {
            nsf.printStackTrace();
        }
    }

    public void invokeMethods() {
        Class workerClass = Worker.class;
        Worker worker = new Worker();
        Method[] methods = workerClass.getMethods();

        try {
        for (Method method : methods) {
            method.setAccessible(true);
            switch (method.getName()) {
                case "showRandomInt" :
                    System.out.println("without params: " +
                            method.invoke(worker));
                    break;
                case "firstLetter" :
                    System.out.println("one param: " +
                            method.invoke(worker, "notBubble"));
                    break;
                case "compare" :
                    System.out.println("two params: " +
                            method.invoke(worker, "one string", "another string"));
            }
        }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void setValue() {
        Class bubbleClass = BubbleClass.class;

        try {
        Field emptyField = bubbleClass.getField("emptyField");
        emptyField.setAccessible(true);
        emptyField.set(null, "static-x");
        System.out.println(emptyField.get(null));

        } catch (Exception nsf) {
            nsf.printStackTrace();
        }
    }

    public void invokeSpecificMethods() {
        Class workerClass = Worker.class;
        Worker worker = new Worker();
        String[] parameters = new String[] {"one", "another", "string"};

        try {
        Method myMethod = workerClass.getDeclaredMethod("myMethod", String.class, int[].class);
        myMethod.invoke(worker, "string", new int[]{0, 5});

        Method myStringMethod = workerClass.getDeclaredMethod("myStringMethod", String[].class);
        myStringMethod.invoke(worker, new Object[]{parameters});

        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    public void showInformationAboutClass(Object object) {
        Class objectClass = object.getClass();
        System.out.println(objectClass.getSimpleName());

        System.out.println("Fields: ");
        Field[] fields = objectClass.getDeclaredFields();

        for (Field field : fields) {
            System.out.println(field.getType().getSimpleName() + " " + field.getName());
        }

        Method[] methods = objectClass.getMethods();
        System.out.println("Methods:");

        for (Method method : methods) {
            System.out.println(showInfoAboutMethod(method));
        }

    }

    private String showInfoAboutMethod(Method method) {
        StringBuilder stringBuilder = new StringBuilder(method.getName());
        stringBuilder.append(" with " + method.getParameterCount()
                + " parameters ");

        Class<?>[] params = method.getParameterTypes();
        for (Class param : params) {
            stringBuilder.append(param.getSimpleName());
            stringBuilder.append(" ");
        }

        stringBuilder.append(" and return type ");
        stringBuilder.append(method.getReturnType().getSimpleName());




        return stringBuilder.toString();
    }
}
