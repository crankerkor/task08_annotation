package com.oleksandr.annotations;

import java.util.Random;

public class Worker {
    private String stringField;
    private int anInt;

    public int showRandomInt() {
        Random rand = new Random();
        return rand.nextInt(15);
    }

    public char firstLetter(String string) {
        return string.charAt(0);
    }

    public String compare(String oneString, String anotherString) {
        String result;
        if (oneString.compareTo(anotherString) == 0) {
            result = oneString + " is equal to " + anotherString;
        } else {
            result = oneString + " is not equal to " + anotherString;;
        }

        return result;
    }

    public void myMethod(String a, int... args) {
        System.out.println(a);
        int i = 0;

        for (int elem : args) {
            System.out.println(i + ") " + elem);
            i++;
        }
    }

    public void myStringMethod(String... args) {
        for (String str : args) {
            System.out.println(str);
        }
    }
}
